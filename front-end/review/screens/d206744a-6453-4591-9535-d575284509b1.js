var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677006825900.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-d206744a-6453-4591-9535-d575284509b1" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Registro" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/d206744a-6453-4591-9535-d575284509b1-1677006825900.css" />\
      <div class="freeLayout">\
      <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="1440.0px" datasizeheight="900.0px" datasizewidthpx="1440.0" datasizeheightpx="900.0" dataX="-0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_2_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Button_2" class="button multiline manualfit firer click commentable non-processed" customid="Sign Up Button"   datasizewidth="402.6px" datasizeheight="43.7px" dataX="178.2" dataY="792.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Button_2_0">Registrarse</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_21" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_2" class="text firer commentable non-processed" customid="Input search"  datasizewidth="447.0px" datasizeheight="48.9px" dataX="156.0" dataY="610.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_7" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="79.3px" datasizeheight="18.0px" dataX="195.3" dataY="602.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">Correo</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_3" class="percentage richtext manualfit firer ie-background commentable non-processed-percentage non-processed" customid="Login your account"   datasizewidth="25.3%" datasizeheight="62.0px" dataX="198.0" dataY="200.8" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_3_0">Registrate en </span><span id="rtr-s-Paragraph_3_1">Ganado&iexcl;Ya!</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_3" class="text firer commentable non-processed" customid="Input search"  datasizewidth="233.0px" datasizeheight="48.9px" dataX="406.0" dataY="294.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_4" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="67.0px" datasizeheight="18.0px" dataX="417.3" dataY="286.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">Apellido</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_4" class="text firer commentable non-processed" customid="Input search"  datasizewidth="233.0px" datasizeheight="48.9px" dataX="116.0" dataY="400.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_5" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="61.0px" datasizeheight="23.0px" dataX="127.3" dataY="392.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_5_0">Telefono</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_5" class="text firer commentable non-processed" customid="Input search"  datasizewidth="233.0px" datasizeheight="48.9px" dataX="406.0" dataY="400.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_6" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="135.0px" datasizeheight="21.0px" dataX="417.3" dataY="392.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_6_0">Fecha de Nacimiento</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_6" class="text firer commentable non-processed" customid="Input search"  datasizewidth="233.0px" datasizeheight="48.9px" dataX="263.0" dataY="501.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_8" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="76.0px" datasizeheight="18.0px" dataX="274.3" dataY="493.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">Documento</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="720.0px" datasizeheight="900.0px" dataX="720.0" dataY="-0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/ba25017b-b49f-4d63-9b83-63af065343cd.jpg" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Image 2"   datasizewidth="177.0px" datasizeheight="204.4px" dataX="271.0" dataY="0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/0a537c47-1a7f-4139-9e33-437dc0c589d5.png" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_3" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="720.0px" datasizeheight="900.0px" dataX="720.0" dataY="-0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/ba25017b-b49f-4d63-9b83-63af065343cd.jpg" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_1" class="text firer commentable non-processed" customid="Input search"  datasizewidth="231.0px" datasizeheight="48.9px" dataX="117.0" dataY="294.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_1" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="67.6px" datasizeheight="18.0px" dataX="128.2" dataY="286.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">Nombre</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_7" class="text firer commentable non-processed" customid="Input search"  datasizewidth="447.0px" datasizeheight="48.9px" dataX="156.0" dataY="705.8" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
        <div id="s-Paragraph_9" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="78.3px" datasizeheight="18.0px" dataX="195.3" dataY="697.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_9_0">Contrase&ntilde;a</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;