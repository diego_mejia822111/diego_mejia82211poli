var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677006825900.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-36d90a29-4b73-4539-a77e-354e7cf0f6c1" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Detalles" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1-1677006825900.css" />\
      <div class="freeLayout">\
      <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Info - right" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Info companies" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_5" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="260.8px" datasizeheight="60.5px" dataX="991.2" dataY="673.8" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_5_0">More than </span><span id="rtr-s-Paragraph_5_1">150 companies</span><span id="rtr-s-Paragraph_5_2"> </span><span id="rtr-s-Paragraph_5_3">from various sectors support us</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_6" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="215.9px" datasizeheight="2.1px" dataX="992.6" dataY="652.9"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="216.39599609375" height="2.5634765625" viewBox="992.5928759882256 652.9354574713904 216.39599609375 2.5634765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_6-36d90" d="M993.3440966913506 654.7477110383651 L1208.2376952387183 653.6866781745154 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-36d90" fill="none" stroke-width="0.5" stroke="#FFFFFF" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="BG"   datasizewidth="532.5px" datasizeheight="904.0px" datasizewidthpx="532.5168198149394" datasizeheightpx="903.9999999999998" dataX="909.5" dataY="-1.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Path_2" class="path firer ie-background commentable non-processed" customid="Path 2"   datasizewidth="373.6px" datasizeheight="7.0px" dataX="6.0" dataY="68.0"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="371.570068359375" height="4.0" viewBox="6.000060867369946 68.00000640645385 371.570068359375 4.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_2-36d90" d="M8.0 70.0 L375.57005589448727 70.0 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-36d90" fill="none" stroke-width="3.0" stroke="#C7751A" stroke-linecap="square"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      <div id="s-Path_3" class="path firer ie-background commentable non-processed" customid="Path 2"   datasizewidth="377.2px" datasizeheight="7.0px" dataX="542.6" dataY="70.1"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="375.190185546875" height="4.0" viewBox="542.5514524689323 70.14673492207883 375.190185546875 4.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_3-36d90" d="M544.5515585169419 72.14669669950081 L915.7415771484375 72.14669669950081 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-36d90" fill="none" stroke-width="3.0" stroke="#C7751A" stroke-linecap="square"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      <div id="s-Path_4" class="path firer ie-background commentable non-processed" customid="Path 2"   datasizewidth="380.8px" datasizeheight="7.0px" dataX="4.0" dataY="117.0"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="378.809814453125" height="4.0" viewBox="4.000060867370097 117.00000640645385 378.809814453125 4.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_4-36d90" d="M6.0 119.0 L380.8099813685044 119.0 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-36d90" fill="none" stroke-width="3.0" stroke="#C7751A" stroke-linecap="square"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      <div id="s-Path_5" class="path firer ie-background commentable non-processed" customid="Path 2"   datasizewidth="380.8px" datasizeheight="7.0px" dataX="540.2" dataY="117.0"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="378.809814453125" height="4.0" viewBox="540.2150329201265 117.00000640645382 378.809814453125 4.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_5-36d90" d="M542.2149720527568 119.0 L917.0249534212617 119.0 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-36d90" fill="none" stroke-width="3.0" stroke="#C7751A" stroke-linecap="square"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 3"   datasizewidth="928.7px" datasizeheight="64.0px" datasizewidthpx="928.7415771484372" datasizeheightpx="64.00000000000003" dataX="-0.0" dataY="64.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_3_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="shapewrapper-s-Ellipse_2" customid="Ellipse 2" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="176.5px" datasizeheight="152.0px" datasizewidthpx="176.4565492011934" datasizeheightpx="151.99999999999997" dataX="398.8" dataY="20.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
              <g>\
                  <g clip-path="url(#clip-s-Ellipse_2)">\
                          <ellipse id="s-Ellipse_2" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 2" cx="88.2282746005967" cy="75.99999999999999" rx="88.2282746005967" ry="75.99999999999999">\
                          </ellipse>\
                  </g>\
              </g>\
              <defs>\
                  <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                          <ellipse cx="88.2282746005967" cy="75.99999999999999" rx="88.2282746005967" ry="75.99999999999999">\
                          </ellipse>\
                  </clipPath>\
              </defs>\
          </svg>\
          <div class="paddingLayer">\
              <div id="shapert-s-Ellipse_2" class="content firer" >\
                  <div class="valign">\
                      <span id="rtr-s-Ellipse_2_0"></span>\
                  </div>\
              </div>\
          </div>\
      </div>\
\
      <div id="s-Image_3" class="image firer click mouseenter mouseleave ie-background commentable non-processed" customid="Image 3"   datasizewidth="225.0px" datasizeheight="225.0px" dataX="374.5" dataY="-16.5"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/182dd2ae-c825-4057-8e63-6cd412677099.png" />\
        	</div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_12" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="131.0px" datasizeheight="64.0px" dataX="216.0" dataY="64.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_12_0">Comprar</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_6" class="richtext manualfit firer click mouseenter mouseleave ie-background commentable non-processed" customid="Paragraph"   datasizewidth="135.0px" datasizeheight="64.0px" dataX="624.0" dataY="64.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_6_0">Mi perfil</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 3"   datasizewidth="928.7px" datasizeheight="53.0px" datasizewidthpx="928.7415771484368" datasizeheightpx="53.0" dataX="-8.9" dataY="850.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_4_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="131.0px" datasizeheight="50.0px" dataX="121.9" dataY="850.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">Comprar</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_2" class="richtext manualfit firer click ie-background commentable non-processed" customid="Paragraph"   datasizewidth="131.0px" datasizeheight="50.0px" dataX="405.2" dataY="850.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_2_0">Home</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_3" class="richtext manualfit firer click ie-background commentable non-processed" customid="Paragraph"   datasizewidth="131.0px" datasizeheight="50.0px" dataX="696.0" dataY="850.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_3_0">Mi perfil</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 1"   datasizewidth="312.0px" datasizeheight="552.0px" datasizewidthpx="312.0" datasizeheightpx="552.0000000000001" dataX="46.0" dataY="228.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_7" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 7"   datasizewidth="388.0px" datasizeheight="73.0px" datasizewidthpx="387.99999999999994" datasizeheightpx="72.99999999999989" dataX="453.1" dataY="228.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_7_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Holstein"   datasizewidth="145.8px" datasizeheight="73.0px" dataX="574.2" dataY="228.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_4_0">Holstein</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_7" class="richtext manualfit firer ie-background commentable non-processed" customid="$2,300,000"   datasizewidth="156.0px" datasizeheight="73.0px" dataX="663.0" dataY="645.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_7_0">$2,300,000</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="312.0px" datasizeheight="552.0px" dataX="46.0" dataY="228.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/9ba4b1cd-907a-4b94-86b5-cef7ff5d107c.jpg" />\
        	</div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_8" class="richtext manualfit firer ie-background commentable non-processed" customid="- Descripci&oacute;n: Tambi&eacute;n co"   datasizewidth="368.1px" datasizeheight="234.0px" dataX="473.0" dataY="348.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_8_0">- Descripci&oacute;n:</span><span id="rtr-s-Paragraph_8_1"> </span><span id="rtr-s-Paragraph_8_2">Tambi&eacute;n conocida como vaca frisona, procede de regiones bajas de Sajonia y SchlewiwgHolstein , en Alemania y Paises bajos.<br /><br />Se destaca por su gran producci&oacute;n de leche aunque con menor tenor graso que las dem&aacute;s razas<br />lecheras .Es de gran tama&ntilde;o, tiene pelaje blanco con manchas negras, cuernos medianos,<br />extremidades blancas y buena adaptabilidad al h&aacute;bitat.</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_9" class="richtext manualfit firer ie-background commentable non-processed" customid="- Nota: Todos los ejempla"   datasizewidth="348.0px" datasizeheight="57.0px" dataX="473.0" dataY="569.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_9_0">- Nota:</span><span id="rtr-s-Paragraph_9_1"> </span><span id="rtr-s-Paragraph_9_2">Todos los ejemplares son vendidos de la misma edad (6 meses) y con un peso promedio de 190 kilogramos</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Button_1" class="button multiline manualfit firer click commentable non-processed" customid="Sign Up Button"   datasizewidth="368.1px" datasizeheight="43.7px" dataX="458.2" dataY="736.3" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Button_1_0">Comprar</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="shapewrapper-s-Ellipse_1" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="45.0px" datasizeheight="35.0px" datasizewidthpx="44.99999999999966" datasizeheightpx="35.000000000000114" dataX="498.0" dataY="664.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
              <g>\
                  <g clip-path="url(#clip-s-Ellipse_1)">\
                          <ellipse id="s-Ellipse_1" class="ellipse shape non-processed-shape manualfit firer mouseenter mouseleave commentable non-processed" customid="Ellipse 1" cx="22.49999999999983" cy="17.500000000000057" rx="22.49999999999983" ry="17.500000000000057">\
                          </ellipse>\
                  </g>\
              </g>\
              <defs>\
                  <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                          <ellipse cx="22.49999999999983" cy="17.500000000000057" rx="22.49999999999983" ry="17.500000000000057">\
                          </ellipse>\
                  </clipPath>\
              </defs>\
          </svg>\
          <div class="paddingLayer">\
              <div id="shapert-s-Ellipse_1" class="content firer" >\
                  <div class="valign">\
                      <span id="rtr-s-Ellipse_1_0">+</span>\
                  </div>\
              </div>\
          </div>\
      </div>\
      <div id="shapewrapper-s-Ellipse_3" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_3 non-processed"   datasizewidth="45.0px" datasizeheight="35.0px" datasizewidthpx="44.99999999999966" datasizeheightpx="35.000000000000114" dataX="618.0" dataY="664.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_3" class="svgContainer" style="width:100%; height:100%;">\
              <g>\
                  <g clip-path="url(#clip-s-Ellipse_3)">\
                          <ellipse id="s-Ellipse_3" class="ellipse shape non-processed-shape manualfit firer mouseenter mouseleave commentable non-processed" customid="Ellipse 1" cx="22.49999999999983" cy="17.500000000000057" rx="22.49999999999983" ry="17.500000000000057">\
                          </ellipse>\
                  </g>\
              </g>\
              <defs>\
                  <clipPath id="clip-s-Ellipse_3" class="clipPath">\
                          <ellipse cx="22.49999999999983" cy="17.500000000000057" rx="22.49999999999983" ry="17.500000000000057">\
                          </ellipse>\
                  </clipPath>\
              </defs>\
          </svg>\
          <div class="paddingLayer">\
              <div id="shapert-s-Ellipse_3" class="content firer" >\
                  <div class="valign">\
                      <span id="rtr-s-Ellipse_3_0">-</span>\
                  </div>\
              </div>\
          </div>\
      </div>\
      <div id="s-Paragraph_11" class="richtext manualfit firer ie-background commentable non-processed" customid="1"   datasizewidth="76.8px" datasizeheight="36.0px" dataX="543.0" dataY="663.5" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_11_0">1</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_10" class="richtext manualfit firer ie-background commentable non-processed" customid="Cantidad disponible: 17"   datasizewidth="243.2px" datasizeheight="39.0px" dataX="551.4" dataY="313.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_10_0">Cantidad disponible:</span><span id="rtr-s-Paragraph_10_1"> </span><span id="rtr-s-Paragraph_10_2">17</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="shapewrapper-s-Ellipse_4" customid="Ellipse 4" class="shapewrapper shapewrapper-s-Ellipse_4 non-processed"   datasizewidth="98.4px" datasizeheight="92.5px" datasizewidthpx="98.39251397362227" datasizeheightpx="92.50000000000034" dataX="841.1" dataY="128.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_4" class="svgContainer" style="width:100%; height:100%;">\
              <g>\
                  <g clip-path="url(#clip-s-Ellipse_4)">\
                          <ellipse id="s-Ellipse_4" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 4" cx="49.196256986811136" cy="46.25000000000017" rx="49.196256986811136" ry="46.25000000000017">\
                          </ellipse>\
                  </g>\
              </g>\
              <defs>\
                  <clipPath id="clip-s-Ellipse_4" class="clipPath">\
                          <ellipse cx="49.196256986811136" cy="46.25000000000017" rx="49.196256986811136" ry="46.25000000000017">\
                          </ellipse>\
                  </clipPath>\
              </defs>\
          </svg>\
          <div class="paddingLayer">\
              <div id="shapert-s-Ellipse_4" class="content firer" >\
                  <div class="valign">\
                      <span id="rtr-s-Ellipse_4_0"></span>\
                  </div>\
              </div>\
          </div>\
      </div>\
\
      <div id="s-Image_4" class="image firer click mouseenter mouseleave ie-background commentable non-processed" customid="Image 4"   datasizewidth="42.6px" datasizeheight="45.6px" dataX="861.8" dataY="151.5"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/dfe8d16c-7b96-46f5-b953-530ddc431d0c.png" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Image 2"   datasizewidth="532.0px" datasizeheight="904.0px" dataX="909.7" dataY="0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/0a376d99-abf8-4347-9093-39ffef99a318.jpg" />\
        	</div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;